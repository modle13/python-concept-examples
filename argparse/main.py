import argparse
import logging
import sys

logger = logging.getLogger(__name__)


def parse_args(args):
    """
    converts command line input vars to an argparse object
    """
    parser = define_parser()
    args = parser.parse_args()
    return args


def define_parser():
    """
    sets up argparse configuration
    """
    parser = argparse.ArgumentParser(description="Map Manage deploy configuration.")
    parser.add_argument('--debug', action='store_true', help="Display debug logging.")
    parser.add_argument(
        '-s',
        metavar='SOME_VAR',
        type=str,
        required=True,
        help="Some required input variable."
    )
    return parser


def configure_logging(args):
    """
    sets up logger
    """
    logging.basicConfig(level=(logging.DEBUG if args.debug else logging.INFO))
    # if using requests
    #requests_log = logging.getLogger('requests.packages.urllib3')
    #requests_log.setLevel(logging.DEBUG if args.debug else logging.INFO)
    #requests_log.propagate = True


def run():
    args = parse_args(sys.argv[1:])
    configure_logging(args)
    print(args)


if __name__ == '__main__':
    run()
