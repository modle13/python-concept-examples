"""common file operations"""

import os
import shutil

def prepare_output_dir(output_dir):
    """
    prepare output dir for write
    """
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    os.makedirs(output_dir)
