"""Loads config data from file system"""
import glob
import logging
import os
import yaml
from yaml import Loader

from nexus_manager.settings import config

logger = logging.getLogger(__name__)

common_data = {}
repo_data = []
role_data = []


def init_parser(subparsers):
    """
    argparse definitions for this submodule
    these are consumed by the top level script
    """
    subparsers.add_parser('load', help='Refresh loaded data from dirs.')


def do_load(args):
    """
    orchestrates data loading calls
    """
    load_repos()
    load_roles()


def load_repos():
    """
    entry point for repo data load
    """
    load_files(config.repos_dir, repo_data)
    load_common_repo_data()
    apply_common_repo_data()


def load_roles():
    """
    entry point for role data load
    """
    load_files(config.roles_dir, role_data)


def load_common_repo_data():
    """
    entry point for common repo data load
    """
    target_dir = f'{config.checkout_dir}/{config.repos_common_dir}'
    common_repos = os.listdir(target_dir)
    logger.debug(f'common repos are {common_repos}')
    for entry in common_repos:
        logger.debug(f'entry is {entry}')
        if '.yml' not in entry:
            continue
        with open(f'{target_dir}/{entry}') as datafile:
            data = yaml.load(datafile.read(), Loader=Loader)
            logger.debug(f'data is {data}')
            entry_name = entry.replace('.yml', '')
            common_data[entry_name] = data


def apply_common_repo_data():
    """
    apply common data for repo format
    """
    for entry in repo_data:
        common_data_name = entry['format'].split('/').pop()
        common = common_data[common_data_name]
        entry.update(common)


# TODO: needs file presence checks and error handling
def load_files(dirname, data_collection):
    """
    loads any type of data file given a target dir and a target collection
    """
    files = find_files(dirname)
    logger.debug(f'files are {files}')
    for entry in files:
        with open(entry) as datafile:
            data = yaml.load(datafile.read(), Loader=Loader)
            logger.debug(f'data is {data}')
            data_collection.append(data)


def find_files(dirname):
    """
    verifies data dir exists and collects list of any *.yml files found
    """
    target_dir = f'{config.checkout_dir}/{dirname}'
    logger.debug(f'checking target_dir: {target_dir}')
    if not os.path.exists(target_dir):
        logger.error(f'expected dir but did not find it: {target_dir}')
        exit(1)
    # get a list of all files in path; this will return an empty list if parent dirs don't exist
    file_glob = glob.glob(f'{target_dir}/**/*', recursive=True)
    # just get the yml files
    files = [i for i in file_glob if i.endswith('.yml')]
    return files
