#!/usr/bin/env python

# Simple pygame program

# Import and initialize the pygame library
import pygame
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
)

pygame.init()

# set up the drawing window
screen = pygame.display.set_mode([200, 200])

# set up the font
font = pygame.font.Font('freesansbold.ttf', 20)

# set up colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# get rect for text
text_rect = screen.get_rect(topleft=(0, 0))

# set up value holders
x_value = 0
y_value = 0

# set up a clock to handle framereate
clock = pygame.time.Clock()

# run until the user asks to quit
running = True
while running:
    # Did the user click the window close button?
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # adjust coordinates based on input
    pressed_keys = pygame.key.get_pressed()
    if pressed_keys[K_UP]:
        y_value += 1
    if pressed_keys[K_DOWN]:
        y_value -= 1
    if pressed_keys[K_LEFT]:
        x_value -= 1
    if pressed_keys[K_RIGHT]:
        x_value += 1

    # fill the background with black
    screen.fill(BLACK)

    # draw a solid blue circle in the center
    #pygame.draw.circle(screen, (0, 0, 255), (250, 250), 75)

    # render the text into the font object
    text_to_render = f'{x_value},{y_value}'
    text_surf = font.render(text_to_render, True, WHITE, BLACK)

    # draw the text surface to the screen
    screen.blit(text_surf, text_rect)

    # flip the display; i.e. make it visible
    pygame.display.flip()

    # set frames allowed per second for the next frame
    clock.tick(10)


# Done! Time to quit.
pygame.quit()
