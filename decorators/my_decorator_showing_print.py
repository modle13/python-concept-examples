#!/usr/bin/env python

import sys

# code for testing decorator chaining
def decor2(func):
    def inner():
        x = func()
        #return x * x
        return 'no from decorator 2 ' + x
    return inner
 
def decor1(func):
    def inner():
        x = func()
        #return 2 * x
        return 'yes from decorator 1 ; input is ' + x
    return inner
 
@decor2
@decor1
def num():
    return sys.argv[1]
 
print(num())
