from blessings import Terminal

t = Terminal()

avar = 'asdf'

print (t.bold('Hi there!'))
print (t.bold_red_on_bright_green(f'It hurts my {avar}!'))

with t.location(0, t.height - 1):
    print ('This is at the bottom.')

def a_func():
    print("some things")