#!/usr/bin/python

# multithreading and you

import thread
import time

# Define a function for the thread
def print_time( threadName, delay):
    count = 0
    while count < 5:
        time.sleep(delay)
        count += 1
        print "%s: %s" % ( threadName, time.ctime(time.time()) )


# Create two threads as follows
try:
    # 2nd param is a tuple? name and execution delay?
    thread.start_new_thread(print_time, ("Thread-1", 1,))
    thread.start_new_thread(print_time, ("Thread-2", 2,))
    thread.start_new_thread(print_time, ("Thread-3", 3,))
    thread.start_new_thread(print_time, ("Thread-4", 4,))
    thread.start_new_thread(print_time, ("Thread-5", 5,))
    thread.start_new_thread(print_time, ("Thread-6", 6,))
except:
    print "Error: unable to start thread"

while 1:
    pass
