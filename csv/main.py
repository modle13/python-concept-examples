import csv

files = [
    {'name': 'input_data/file1.csv', 'headers': False,},
    {'name': 'input_data/file2.csv', 'headers': False,},
    {'name': 'input_data/file3.csv', 'headers': True,},
]

def csv_reader(filename, headers=False) -> None:
    with open(filename) as csv_file:
        if headers:
            csv_reader = csv.DictReader(csv_file, delimiter=',')
        else:
            csv_reader = csv.reader(csv_file, delimiter=',')

        for row in csv_reader:
            print(row)

for entry in files:
    csv_reader(entry.get('name'), headers=entry.get('headers'))
