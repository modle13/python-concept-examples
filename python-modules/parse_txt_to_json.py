#!/usr/bin/python3

import json

# could get a list of files from dir, but they're not all of interest
files = []
rootPath = '/home/matt/projects/writing/book-reports/'
files.append('the-power-of-no-notes.txt')
files.append('grit-notes.txt')
files.append('crake-and-oryx-notes.txt')
files.append('the-spiritual-teachings-of-marcus-aurelius.txt')

def starts_with_number(string_to_test):
    return string_to_test[0].isdigit()

def get_my_file(path):
    return open(path, 'r')

def split_string_on_first_space(string_to_split):
    return string_to_split.split(' ', 1)

def parse_the_file(filename, entries):
    try:
        my_file = get_my_file('{}{}'.format(rootPath, filename))
        theDict = {}
        for line in my_file:
            if (theDict):
                entries.append(theDict)
            if starts_with_number(line):
                theDict = {}
                split_record = split_string_on_first_space(line)
                theDict['number'] = split_record[0]
                theDict['quote'] = split_record[1]
            else:
                theDict['quote'] += line
        if (theDict):
            entries.append(theDict)
    except:
        print ('failed on file {}'.format(filename))

def output_the_results(filename, entries):
    with open('{}{}.json'.format(rootPath, filename), 'w') as data_file:
        json.dump(entries, data_file)

for filename in files:
    entries = []
    parse_the_file(filename, entries)
    output_the_results(filename, entries)
