import configparser
import logging
import os
import sys


logger = logging.getLogger(__name__)
config = configparser.ConfigParser(interpolation=None)

"""
myfile.conf contents similar to:

```
[global]
myvar=value
```
"""
if len(sys.argv) == 2:
    filename = sys.argv[1]
else:
    logger.error('incorrect number of params; expected filename param as input')
    exit(1)

try:
    config.read([
        os.path.join(os.path.expanduser('~'), filename),
        filename
    ])
except (configparser.ParsingError, ValueError) as e:
    logger.error(f'configparser failed to handle one or more input vars\n{e}')


print(list(dict(config.items('global')).keys()))


""" alternately optionally read from user dir with:
    config.read([
        os.path.join(os.path.expanduser('~'), filename),
        filename
    ])
"""
