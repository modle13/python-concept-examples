#!/usr/bin/env python

"""
Rect built-ins make this operation trivial, but the goal is to
work out mathematically how to compare rects using dict lookup
instead of a loop.
"""

import json
import math
import pygame
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    # vim movement keys
    K_h,
    K_j,
    K_k,
    K_l,
)

# set up colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (20, 200, 20)

GRAY = (100, 100, 100)
BLUE = (20, 150, 200)
LIGHT_BLUE = (173, 216, 230)
OFF_WHITE = (251, 247, 245)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)


WIDTH = 192
HEIGHT = 192

move_speed = 4

rect_side = 32
start_x = 64
start_y = 96
end_x = start_x + rect_side
end_y = start_y + rect_side

rect_topleft_points = {
        f'{start_x},{start_y}': 'quad1,blue',
        f'{end_x},{start_y}': 'quad2,yellow',
        f'{end_x},{end_y}': 'quad3,red',
        f'{start_x},{end_y}': 'quad4,green',
}

rect_colors = {
        f'{start_x},{start_y}': BLUE,
        f'{end_x},{start_y}': YELLOW,
        f'{end_x},{end_y}': RED,
        f'{start_x},{end_y}': GREEN,
}

player_rect = pygame.Rect(
        (start_x - move_speed * 4, start_y),
        (rect_side, rect_side)
    )

display_rects = []

def main():
    pygame.init()

    # set up the drawing window
    screen = pygame.display.set_mode([WIDTH, HEIGHT])

    # set up the font
    font = pygame.font.Font('freesansbold.ttf', 20)

    # get rect for text
    pos_rect = screen.get_rect(topleft=(0, 0))
    snap_rect = screen.get_rect(topleft=(WIDTH // 2, 0))
    match_rect = screen.get_rect(topleft=(0, 20))
    value_rect = screen.get_rect(topleft=(20, 40))

    # set up a clock to handle framereate
    clock = pygame.time.Clock()
    make_rects(screen)

    # run until the user asks to quit
    running = True
    while running:
        # Did the user click the window close button?
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # adjust coordinates based on input
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[K_UP] or pressed_keys[K_j]:
            player_rect.y += -move_speed
        if pressed_keys[K_DOWN] or pressed_keys[K_k]:
            player_rect.y += move_speed
        if pressed_keys[K_LEFT] or pressed_keys[K_h]:
            player_rect.x += -move_speed
        if pressed_keys[K_RIGHT] or pressed_keys[K_l]:
            player_rect.x += move_speed
        
        #quadrant_matched = get_quadrant(player_rect.x, player_rect.y)
        quadrant_matched = check_overlap(player_rect)

        # fill the background with black
        screen.fill(BLACK)

        # draw a solid blue circle in the center
        #pygame.draw.circle(screen, (0, 0, 255), (250, 250), 75)

        # render the text into the font object
        pos_surf = font.render(f'{player_rect.x},{player_rect.y}', True, WHITE, BLACK)
        snap_surf = font.render(f'{snap(player_rect.x)},{snap(player_rect.y)}', True, WHITE, BLACK)
        match_surf = font.render(f'matched?', True, WHITE, BLACK)
        value_surf = font.render(f'{quadrant_matched}', True, WHITE, BLACK)

        # draw the text surface to the screen
        screen.blit(pos_surf, pos_rect)
        screen.blit(snap_surf, snap_rect)
        screen.blit(match_surf, match_rect)
        screen.blit(value_surf, value_rect)

        draw_rects(screen)
        make_grid(screen)

        # flip the display; i.e. make it visible
        pygame.display.flip()

        # set frames allowed per second for the next frame
        clock.tick(10)


    # Done! Time to quit.
    pygame.quit()


def snap(value):
    return math.floor(value / rect_side) * rect_side


def get_quadrant(x, y):
    snap_x = snap(x)
    snap_y = snap(y)
    #print('unit is:',rect_side,' coords:',x,y,' snapped:',snap_x,snap_y)

    return rect_topleft_points.get(f'{snap_x},{snap_y}')


def check_overlap(actor):
    x = actor.x
    y = actor.y

    # get snapped coordinates
    offset_x = actor.width
    offset_y = actor.height
    check_points = {
            'quad1': {'x': snap(x), 'y': snap(y)},
            'quad2': {'x': snap(x) + offset_x, 'y': snap(y)},
            'quad3': {'x': snap(x) + offset_x, 'y': snap(y) + offset_y},
            'quad4': {'x': snap(x), 'y': snap(y) + offset_y},
    }

    # do rectangle math to find most overlapped
    results = {}
    most_overlapped = 'none'
    most_overlapped_area = 0
    for key, value in check_points.items():
        overlapped_with = rect_topleft_points.get(f'{value.get("x")},{value.get("y")}')
        overlap = calculate_rect_overlap(actor, value) if overlapped_with else (0, 0)
        overlap_area = overlap[0] * overlap[1]
        if overlap_area > most_overlapped_area:
            most_overlapped_area = overlap_area 
            most_overlapped = overlapped_with
        results[key] = {
                'overlaps_with': overlapped_with if overlap_area else 'no',
                'overlap_dims': overlap if overlap_area else 'no',
                'overlap_area': overlap_area,
        }
    print(json.dumps(results, indent=4, sort_keys=True))
    return most_overlapped


def calculate_rect_overlap(actor, target):
    x_overlap = max(0, min(target.get('x') + rect_side, actor.x + actor.width ) - max(actor.x, target.get('x')))
    y_overlap = max(0, min(target.get('y') + rect_side, actor.y + actor.height) - max(actor.y, target.get('y')))
    return x_overlap, y_overlap


def make_grid(screen):
    start_height = rect_side * 2
    y = start_height
    # draw horizontal lines
    while y < HEIGHT:
        pygame.draw.line(
            screen, WHITE,
            (0, y),
            (WIDTH, y),
            width=1
        )
        y += rect_side

    x = rect_side
    # draw vertical lines
    while x < WIDTH:
        pygame.draw.line(
            screen, WHITE,
            (x, start_height),
            (x, HEIGHT),
            width=1
        )
        x += rect_side


def draw_rects(screen):
    # draw display rects
    for entry in display_rects:
        pygame.draw.rect(screen, rect_colors[f'{entry.x},{entry.y}'], entry, border_radius=10)
    # draw player rect
    pygame.draw.rect(screen, WHITE, player_rect, border_radius=10)


def make_rects(screen):
    # draw target rects
    for coord in rect_topleft_points.keys():
        x, y = coord.split(',')
        display_rects.append(pygame.Rect((int(x), int(y)), (rect_side, rect_side)))


if __name__ == '__main__':
    main()
