#!/usr/bin/env python

"""
Rect built-ins make this operation trivial, but the goal is to
work out mathematically how to compare rects using dict lookup
instead of a loop.
"""

import math
import pygame
from pygame.locals import (
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    # vim movement keys
    K_h,
    K_j,
    K_k,
    K_l,
)

# set up colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (20, 200, 20)

GRAY = (100, 100, 100)
BLUE = (20, 150, 200)
LIGHT_BLUE = (173, 216, 230)
OFF_WHITE = (251, 247, 245)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)


WIDTH = 192
HEIGHT = 192

move_speed = 4

rect_side = 32
start_x = 64
start_y = 96
end_x = start_x + rect_side
end_y = start_y + rect_side

rect_topleft_points = {
        f'{start_x},{start_y}': 'quad1,blue',
        f'{end_x},{start_y}': 'quad2,yellow',
        f'{end_x},{end_y}': 'quad3,red',
        f'{start_x},{end_y}': 'quad4,green',
}

rect_colors = {
        f'{start_x},{start_y}': BLUE,
        f'{end_x},{start_y}': YELLOW,
        f'{end_x},{end_y}': RED,
        f'{start_x},{end_y}': GREEN,
}


def main():
    pygame.init()

    # set up the drawing window
    screen = pygame.display.set_mode([WIDTH, HEIGHT])

    # set up the font
    font = pygame.font.Font('freesansbold.ttf', 20)

    # get rect for text
    pos_rect = screen.get_rect(topleft=(0, 0))
    snap_rect = screen.get_rect(topleft=(WIDTH // 2, 0))
    match_rect = screen.get_rect(topleft=(0, 20))
    value_rect = screen.get_rect(topleft=(20, 40))

    # set up value holders
    x_value = start_x - move_speed * 4
    y_value = start_y

    # set up a clock to handle framereate
    clock = pygame.time.Clock()

    # run until the user asks to quit
    running = True
    while running:
        # Did the user click the window close button?
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        # adjust coordinates based on input
        pressed_keys = pygame.key.get_pressed()
        if pressed_keys[K_UP] or pressed_keys[K_j]:
            y_value += -move_speed
        if pressed_keys[K_DOWN] or pressed_keys[K_k]:
            y_value += move_speed
        if pressed_keys[K_LEFT] or pressed_keys[K_h]:
            x_value += -move_speed
        if pressed_keys[K_RIGHT] or pressed_keys[K_l]:
            x_value += move_speed
        
        quadrant_matched = get_quadrant(x_value, y_value)

        # fill the background with black
        screen.fill(BLACK)

        # draw a solid blue circle in the center
        #pygame.draw.circle(screen, (0, 0, 255), (250, 250), 75)

        # render the text into the font object
        pos_surf = font.render(f'{x_value},{y_value}', True, WHITE, BLACK)
        snap_surf = font.render(f'{snap(x_value)},{snap(y_value)}', True, WHITE, BLACK)
        match_surf = font.render(f'matched?', True, WHITE, BLACK)
        value_surf = font.render(f'{quadrant_matched}', True, WHITE, BLACK)

        # draw the text surface to the screen
        screen.blit(pos_surf, pos_rect)
        screen.blit(snap_surf, snap_rect)
        screen.blit(match_surf, match_rect)
        screen.blit(value_surf, value_rect)

        make_rects(screen, x_value, y_value)
        make_grid(screen)

        # flip the display; i.e. make it visible
        pygame.display.flip()

        # set frames allowed per second for the next frame
        clock.tick(10)


    # Done! Time to quit.
    pygame.quit()


def snap(value):
    return math.floor(value / rect_side) * rect_side


def get_quadrant(x, y):
    snap_x = snap(x)
    snap_y = snap(y)
    print('unit is:',rect_side,' coords:',x,y,' snapped:',snap_x,snap_y)
    return rect_topleft_points.get(f'{snap_x},{snap_y}')


def make_grid(screen):
    start_height = rect_side * 2
    y = start_height
    # draw horizontal lines
    while y < HEIGHT:
        pygame.draw.line(
            screen, WHITE,
            (0, y),
            (WIDTH, y),
            width=1
        )
        y += rect_side

    x = rect_side
    # draw vertical lines
    while x < WIDTH:
        pygame.draw.line(
            screen, WHITE,
            (x, start_height),
            (x, HEIGHT),
            width=1
        )
        x += rect_side


def make_rects(screen, player_x, player_y):
    # draw target rects
    for coord in rect_topleft_points.keys():
        x, y = coord.split(',')
        #print(x, y)
        new_rect = pygame.Rect((int(x), int(y)), (rect_side, rect_side))
        pygame.draw.rect(screen, rect_colors[coord], new_rect, border_radius=10)

    # draw player rect
    player_rect = pygame.Rect((player_x, player_y), (rect_side, rect_side))
    pygame.draw.rect(screen, WHITE, player_rect, border_radius=10)


if __name__ == '__main__':
    main()
