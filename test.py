import sys

matches = ''
alpha = 0
num = 0

for c in sys.argv[1]+' ':
    if alpha+num == 6 and (not c or c==' '):
        if alpha != 0 and num != 0:
            print(matches)
            alpha = 0
            num = 0
            matches = ''

    if c:
        if c.isalpha():
            alpha += 1
            matches = f'{matches}{c}'
        try:
            int(c)
            num += 1
            matches = f'{matches}{c}'
        except:
            pass
    else:
        num = 0
        alpha = 0
        matches = ''
